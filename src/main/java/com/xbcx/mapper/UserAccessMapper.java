package com.xbcx.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xbcx.entity.UserAccess;

/**
 * @author 伍炳清
 * @date 2020-10-15 20:30
 */
//接口只需继承Mapper
public interface UserAccessMapper extends BaseMapper<UserAccess> {

}
