package com.xbcx.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xbcx.entity.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author 伍炳清
 * @date 2020-10-15 20:30
 */
//接口只需继承Mapper
public interface UserMapper extends BaseMapper<User> {

//    /**
//     * 用户添加权限的方法
//     *
//     * @param userPhone
//     * @param accessIds 权限集合
//     * @return
//     */
//    @Transactional(propagation = Propagation.REQUIRED)
//    Integer addAccess(@Param("userPhone") String userPhone, @Param("accessIds") List<Integer> accessIds);

//    /**
//     * 查询用户是否有权限
//     * @param phone
//     * @param accessId
//     * @return
//     */
//    @Transactional(propagation = Propagation.REQUIRED)
//    Integer  selectAccess(@Param("phone") String phone,@Param("accessId")Integer accessId);

//    /**
//     * 清空权限
//     * @param phone
//     * @return
//     */
//    @Transactional(propagation = Propagation.REQUIRED)
//    Integer deleteAccess(String phone);

}
