package com.xbcx.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.entity.User;
import com.xbcx.exception.CustomerException;
import com.xbcx.service.UserService;
import com.xbcx.util.JsonResult;
import com.xbcx.util.LayuiReplay;
import com.xbcx.util.TokenUtil;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 15:52
 */
@RestController
@RequestMapping("/User")
@CrossOrigin
public class UserController {
    @Resource
    private UserService userService;
    JsonResult jsonResult = new JsonResult();

    @RequestMapping("/addUser")
    public Map<String, Object> add(@RequestParam Map<String, Object> map) throws CustomerException {

        Integer tId = Integer.valueOf((String) map.get("tId"));
        String name = (String) map.get("name");
        String username = (String) map.get("username");
        String password = (String) map.get("password");
        String phone = (String) map.get("phone");
        String accessIds = (String) map.get("accessIds");

        User user = new User(null, name, username, password, phone);
        userService.addUser(tId, user, accessIds);
        jsonResult.setCode("1");
        jsonResult.setMsg("添加成功");
        return jsonResult.getValues();
    }

    @RequestMapping("/deleteUser")
    public Map<String, Object> delete(@RequestParam Integer tId, @RequestParam Integer id) throws CustomerException {

        userService.deleteUser(tId, id);
        jsonResult.setCode("1");
        jsonResult.setMsg("删除成功");
        return jsonResult.getValues();
    }

    @RequestMapping("/updateAccess")
    public Map<String, Object> updateAccess(@RequestParam Integer tId, @RequestParam Integer id, @RequestParam String accessIds) throws CustomerException {
        userService.updateAccess(tId, id, accessIds);
        jsonResult.setCode("1");
        jsonResult.setMsg("修改成功");
        return jsonResult.getValues();
    }

    @RequestMapping("/findAllUser")
    @CrossOrigin
    public LayuiReplay<User> findALl(@RequestParam Integer tId,
                                     @RequestParam Integer page,
                                     @RequestParam Integer limit) throws CustomerException {
        Page<User> allUser = userService.findAllUser(tId, page, limit);
        return new LayuiReplay<>(0, "查询成功", (int) allUser.getTotal(), allUser.getRecords());

    }

    @RequestMapping("/login")
    public Map<String, Object> login(@RequestParam String userName, @RequestParam String passWord) throws CustomerException {
        User login = userService.login(userName, passWord);
        //生成token
        String token = TokenUtil.getToken(login.getId(), login.getUsername());
        jsonResult.setData(login);
        jsonResult.setCode("1");
        jsonResult.setMsg("欢迎" + login.getName());
        jsonResult.setToken(token);
        return jsonResult.getValues();
    }


}
