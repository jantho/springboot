package com.xbcx.service.serviceImpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.entity.UserAccess;
import com.xbcx.exception.CustomerException;
import com.xbcx.mapper.UserAccessMapper;
import com.xbcx.mapper.UserMapper;
import com.xbcx.entity.User;
import com.xbcx.service.UserService;
import org.springframework.stereotype.Service;


import javax.annotation.Resource;
import java.util.List;

/**
 * @author 伍炳清
 * @date 2021-10-16 14:50
 */

@Service("userService")
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;
    @Resource
    private UserAccessMapper userAccessMapper;


    @Override
    public void addUser(Integer tId, User user, String accessIds) throws CustomerException {
        //查询当前用户是否有权限
        UserAccess a = userAccessMapper.selectOne(new LambdaQueryWrapper<UserAccess>()
                .eq(UserAccess::getUserId, tId)
                .eq(UserAccess::getAccessId, 1)
                .last("limit 1"));
        if (a == null) {
            throw new CustomerException("您没有新增权限");
        }
        //查询手机号或者用户名是否重复
        User user1 = userMapper.selectOne(new LambdaQueryWrapper<User>()
                .eq(User::getPhone, user.getPhone())
                .or().eq(User::getUsername, user.getUsername())
                .last("limit 1"));
        if (user1 != null) {
            throw new CustomerException("用户名或手机号已存在");
        }
        //新增用户
        userMapper.insert(user);
        //查询新用户的id
        User newUser = userMapper.selectOne(new LambdaQueryWrapper<User>()
                .eq(User::getPhone, user.getPhone())
                .eq(User::getUsername, user.getUsername())
                .last("limit 1"));
        //如果权限id集合不为空将用户id和权限id插入中间表
        if (accessIds != null && accessIds.length() > 0) {
            UserAccess userAccess = new UserAccess();
            userAccess.setUserId(newUser.getId());
            String[] access = accessIds.split(",");
            for (String accessId : access) {
                userAccess.setAccessId(Integer.valueOf(accessId));
                userAccessMapper.insert(userAccess);
            }

        }

    }

    @Override
    public User login(String username, String password) throws CustomerException {
        //查询
        User user = userMapper.selectOne(new LambdaQueryWrapper<User>()
                .eq(User::getPassword, password)
                .eq(User::getUsername, username)
                .last("limit 1"));
        if (user == null) {
            throw new CustomerException("用户名或密码错误");
        }
        return user;

    }

    @Override
    public void deleteUser(Integer tId, Integer id) throws CustomerException {
        //查询当前用户是否有权限
        UserAccess a = userAccessMapper.selectOne(new LambdaQueryWrapper<UserAccess>()
                .eq(UserAccess::getUserId, tId)
                .eq(UserAccess::getAccessId, 2)
                .last("limit 1"));
        if (a == null) {
            throw new CustomerException("您没有删除权限");
        }

        //查询用户
        User user = userMapper.selectById(id);
        //先清空要删除用户的权限
        userAccessMapper.delete(new LambdaQueryWrapper<UserAccess>()
                .eq(UserAccess::getUserId, user.getId()));
        //删除用户
        userMapper.deleteById(user.getId());

    }

    @Override
    public void updateAccess(Integer tId, Integer id, String accessIds) throws CustomerException {
        //查询当前用户是否有权限
        UserAccess a = userAccessMapper.selectOne(new LambdaQueryWrapper<UserAccess>()
                .eq(UserAccess::getUserId, tId)
                .eq(UserAccess::getAccessId, 3)
                .last("limit 1"));
        if (a == null) {
            throw new CustomerException("您没有修改权限");
        }
        //先清空在赋新权限
        //查询用户
        User user = userMapper.selectById(id);
        //清空权限
        userAccessMapper.delete(new LambdaQueryWrapper<UserAccess>()
                .eq(UserAccess::getUserId, user.getId()));
        //赋予新权限
        //如果权限id集合不为空将用户id和权限id插入中间表
        if (accessIds != null && accessIds.length() > 0) {
            UserAccess userAccess = new UserAccess();
            userAccess.setUserId(user.getId());
            String[] access = accessIds.split(",");
            for (String accessId : access) {
                System.out.println(accessId);
                userAccess.setAccessId(Integer.valueOf(accessId));
                userAccessMapper.insert(userAccess);
            }

        }


    }

    public Page<User> findAllUser(Integer tId, Integer page, Integer limit) throws CustomerException {
        //查询当前用户是否有权限
        UserAccess a = userAccessMapper.selectOne(new LambdaQueryWrapper<UserAccess>()
                .eq(UserAccess::getUserId, tId)
                .eq(UserAccess::getAccessId, 4)
                .last("limit 1"));
        if (a == null) {
            throw new CustomerException("您没有查询权限");
        }

        Page<User> userPage = new Page<>(page, limit);

        userMapper.selectPage(userPage, new LambdaQueryWrapper<User>().orderByDesc(User::getId));

        return userPage;

    }

    @Override
    public String getAccessIds(Integer id) {
        //查询出所有权限
        List<UserAccess> userAccesses = userAccessMapper.selectList(new LambdaQueryWrapper<UserAccess>()
                .eq(UserAccess::getUserId, id));
        StringBuilder accessIds = new StringBuilder(userAccesses.size());

        for (UserAccess userAccess : userAccesses) {
            if (accessIds.length() == 0) {
                accessIds.append(userAccess);
            } else {
                accessIds.append("," + userAccess);
            }
        }
        return accessIds.toString();
    }


}
