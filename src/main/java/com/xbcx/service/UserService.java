package com.xbcx.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xbcx.exception.CustomerException;
import com.xbcx.entity.User;

/**
 * @author 伍炳清
 * @date 2020-10-16 14:50
 */
public interface UserService {
    /**
     * 新增用户和权限
     */
    public void addUser(Integer tId, User user, String accessIds) throws CustomerException;

    /**
     * 用户登录
     *
     * @param username
     * @param password
     * @return
     */
    User login(String username, String password) throws CustomerException;

    /**
     * 删除用户
     *
     * @param tId
     * @param id
     * @throws CustomerException
     */
    public void deleteUser(Integer tId, Integer id) throws CustomerException;

    /**
     * 修改用户权限
     *
     * @param accessIds
     */
    public void updateAccess(Integer tId, Integer id, String accessIds) throws CustomerException;

    /**
     * 分页查询用户列表
     */
    public Page<User> findAllUser(Integer tId, Integer page, Integer limit) throws CustomerException;

    /**
     * 权限集合查询
     *
     * @param id
     * @return
     */
    String getAccessIds(Integer id);


}
