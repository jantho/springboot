package com.xbcx.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @author 伍炳清
 * @date 2020-10-15 20:17
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

@TableName(value = "table_user_access")
public class UserAccess extends Model<UserAccess> {
    private static final long serialVersionUID = 1L;
    @TableField("user_id")
    private Integer userId;

    @TableField("access_id")
    private Integer accessId;

}
