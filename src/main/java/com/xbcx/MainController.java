package com.xbcx;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


/**
* @Author: 伍炳清
* @Date: 2021/3/23 10:56
*/
@SpringBootApplication
@MapperScan("com.xbcx.mapper")
@ComponentScan("com.xbcx.*")
public class MainController {

    public static void main(String[] args) {
        SpringApplication.run(MainController.class, args);
    }

}

