import com.xbcx.MainController;
import com.xbcx.util.AESUtil;
import com.xbcx.util.TokenUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 10:25
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MainController.class)
public class TestToken {


    @Test
    public void getToken() {

        String xiao = TokenUtil.getToken(1, "小明");

        System.out.println(xiao);

    }

    @Test
    public void decrypt() {

        Map s = TokenUtil.readToken("8vM7J+UMzv6ByVaEc4zNAA==,sVasU5CgXacfmDL9r8OMQA==");

        System.out.println(s);

    }

    @Test
    public void Test() {
        int x = 3, y = 2, z = 5;

        System.out.println(y += z-- / ++x);
    }


}
