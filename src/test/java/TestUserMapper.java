import com.xbcx.MainController;
import com.xbcx.entity.User;
import com.xbcx.exception.CustomerException;
import com.xbcx.mapper.UserMapper;
import com.xbcx.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;

/**
 * @Author: 伍炳清
 * @Date: 2021/3/23 10:25
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MainController.class)
public class TestUserMapper {

    @Resource
    private UserMapper userMapper;
    @Resource
    private UserService userService;

    @Test
    public void addUser() {
        String accessIds = "3";

        User user = new User(null, "3", "3", "3", "3");
        try {
            userService.addUser(1, user, accessIds);
        } catch (CustomerException e) {
            System.out.println(e.getMessage());
        }

    }

    @Test
    public void login() {

        try {
            System.out.println(userService.login("xm", "123"));
        } catch (CustomerException e) {
            System.out.println(e.getMessage());
        }

    }

    @Test
    public void updateAccess() {

        try {
            userService.updateAccess(1,3,"4");
        } catch (CustomerException e) {
            System.out.println(e.getMessage());
        }

    }
    @Test
    public void findPage() {

        try {
            System.out.println(userService.findAllUser(1,1,5));
        } catch (CustomerException e) {
            System.out.println(e.getMessage());
        }

    }
    @Test
    public void Test() {
        int x = 3, y = 2, z = 5;

        System.out.println(y += z-- / ++x);
    }


}
